
# Setting defensive file permissions for wordpress
This guid would showcase how to lock and unlock wordpress files and folders to secure them.
The most important file are .htaccess and wp-config.php in the root directory of the wordpress website
it contains database credentials and it should not have read access to the public, it should have permission of 600, only the owner would be able to read and write.
beaware that there are tools that scan wordpress websites for readable .htaccess and wp-config.php files.

## Resources
Use the links below to convert permissions and understand reccommended wordpress file permissions 
- [Chmod Calculator](https://nodesource.com/products/nsolid)
- [Recommended Wordpress File Permissions](https://wp-rocket.me/blog/wordpress-file-permissions/)


## files and folders to focus
- .htaccess
- wp-config.php
- wp-admin
- wp-content
- wp-inlcudes
- plugins

## steps to lock and unlock files
- login to [aws](https://eu-west-1.console.aws.amazon.com/console)
- navigate to [lightsail](https://lightsail.aws.amazon.com/ls/webapp/home/instances)
- connect/ssh to the worpress instance
- run your commands(below) on the terminal that pops up

### lock and unlock permissions commands
| target file/folder | command to lock | command to unlock |
| ------ | ----- | ------|
|.htaccess | ```sudo chmod 600 /opt/bitnami/apps/wordpress/htdocs/.htaccess``` | ```sudo chmod 755 /opt/bitnami/apps/wordpress/htdocs/.htaccess``` |
|wp-config.php | ```sudo chmod 600 /opt/bitnami/apps/wordpress/htdocs/wp-config.php``` | ``` sudo chmod 775 /opt/bitnami/apps/wordpress/htdocs/wp-config.php ``` |
| plugins |  ```sudo chmod 755 /opt/bitnami/apps/wordpress/htdocs/wp-content/plugins```|  ```sudo chmod 775 /opt/bitnami/apps/wordpress/htdocs/wp-content/plugins``` 
| themes |  ```sudo chmod 755 /opt/bitnami/apps/wordpress/htdocs/wp-content/themes```|  ```sudo chmod 775 /opt/bitnami/apps/wordpress/htdocs/wp-content/themes``` 
| uploads |  ```sudo chmod 755 /opt/bitnami/apps/wordpress/htdocs/wp-content/uploads```|  ```sudo chmod 775 /opt/bitnami/apps/wordpress/htdocs/wp-content/uploads``` 
| wp-admin |  ```sudo chmod 755 /opt/bitnami/apps/wordpress/htdocs/wp-admin```|  ```sudo chmod 775 /opt/bitnami/apps/wordpress/htdocs/wp-admin``` 
| wp-content |  ```sudo chmod 755 /opt/bitnami/apps/wordpress/htdocs/wp-content```|  ```sudo chmod 775 /opt/bitnami/apps/wordpress/htdocs/wp-content``` 

### restart server
```sudo /opt/bitnami/ctlscript.sh restart apache```

### check error logs
```cd /opt/bitnami/apache2/logs/```

```cat error_log```

### install ssl certificate
```sudo /opt/bitnami/bncert-tool```

### ssl cert dir
```cd /opt/bitnami/apache2/conf/```

#### default dir permisions of cert dir
```
drwxr-xr-x 2 bitnami root 4096 Dec 10 14:10 certs
-rw-r----- 1 bitnami root  719 Dec 10 14:10 htaccess.conf
-rw-r--r-- 1 bitnami root 1045 Dec 10 14:16 httpd-app.conf
-rw-r--r-- 1 bitnami root  455 Dec 10 14:16 httpd-prefix.conf
-rw-r--r-- 1 bitnami root  640 Dec 10 14:10 httpd-vhosts.conf
drwxr-xr-x 2 bitnami root 4096 Dec 10 14:10 php-fpm
```


### Verify that the current key matches the certificate file with the following commands. Note that the SHA checksum of the key and certificate must match.
- Check your certificate:
```openssl x509 -in server.crt -pubkey -noout -outform pem | sha256sum```
- Check your key:
```openssl pkey -in server.key -pubout -outform pem | sha256sum```

- The output of the two commands above should match. In case of a mismatch, the wrong key is in use for the certificate and so the Web server will not start until the issue is resolved.
